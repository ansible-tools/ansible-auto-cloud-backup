# Ansible Easy Starter


**TL;DR** Ansible is awesome. And like most awesome tools, it's setup sucks. This project aims to give you a minimum setup required to safely test and easily deploy your playbooks.


## Features

1. Install (and upgrade) the current ansible version locally, avoiding risky system-wide methods like using ~~ppa~~ or ~~sudo pip~~		# TODO

2. Install the current version of vagrant (and virtualbox), a tool for testing your playbooks in a vm

3. Provide a basic ansible setup for safe testing, so you can focus on writing your playbooks instead of configuring ansible (and vagrant)

4. Deploy your tested playbooks to your desktop pc and/or server(s) easily just by changing the value for hosts


## Usage

```bash
git clone https://gitlab.com/ruttercrash/ansible-easy-starter
cd ansible-easy-starter/install_vagrant
./install_vagrant		# enter sudo password
cd ..
vagrant up
./main.yml
```

You should see a succeding ping request to the vagrant vm.

**NOTE:** After doing ```vagrant destroy``` you'll get an ssh warning about a man-in-the-middle attack, which can safely be ignored. Follow the instructions in the warning to remove the offending ssh key with ```ssh-keygen -f ...```


### Deployments

Change ```hosts: vagrant``` in ```main.yml``` to ```hosts: localhost``` to deploy to your desktop pc. To deploy to your servers change it to ```hosts: servers``` and provide the corresponding ip-addresses and ssh-usernames in ```inventory.ini``` under ```[servers]```.


**Have fun!**